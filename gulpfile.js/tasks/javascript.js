/*****************************************
* Dependências para tarefas relacionadas
* aos arquivos de javascript
*****************************************/
var gulp           = require('gulp');

/*****************************************
* Compila os arquivos de JS
*****************************************/
gulp.task('js', function () {
  return gulp.src('./src/js/*.js')
    .pipe(gulp.dest('./dist/assets/js'));
});

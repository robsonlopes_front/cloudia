/*****************************************
* Dependências para tarefas relacionadas
* aos arquivos de estilização (css e scss)
*****************************************/
var gulp           = require('gulp');
var sass           = require('gulp-sass');
var cssmin         = require('gulp-minify-css');

/*****************************************
* Compila os arquivos de CSS
*****************************************/
gulp.task('css', function () {
  return gulp.src('./src/scss/*.scss')
    .pipe(sass({ style: 'compressed' })).on('error', sass.logError)
    .pipe(cssmin())
    .pipe(gulp.dest('./dist/assets/css'));
});

/*****************************************
* Dependências para tarefas default
*****************************************/
var gulp           = require('gulp');
var twig           = require('gulp-twig');

/*****************************************
* Compila o HTML utilizando twig
*****************************************/
gulp.task('html', function(){
  return gulp.src('./src/pages/*.html')
  .pipe(twig({
    errorLogToConsole: true
  }))
  .pipe(gulp.dest('./dist'))
});

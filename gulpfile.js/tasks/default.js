/*****************************************
* Dependências para tarefas default
*****************************************/
var gulp           = require('gulp');
var sass           = require('gulp-sass');
var browserSync    = require('browser-sync').create();

/*****************************************
* Assiste atualizações nos arquivos
*****************************************/
gulp.task('watch', function(){
  gulp.watch('./src/**/*.scss', ['css', 'html', browserSync.reload]);
  gulp.watch('./src/**/*.html', ['html', browserSync.reload]);
  gulp.watch('./src/js/**/*.js', ['js', browserSync.reload]);
});

/*****************************************
* Tarefa default ao executar "gulp"
*****************************************/
gulp.task('default', ['css', 'js', 'html', 'watch'], function(){
  browserSync.init({
    server: {
      baseDir: ['dist']
    },
  })
});
